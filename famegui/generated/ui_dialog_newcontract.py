# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'xx.ui'
##
## Created by: Qt User Interface Compiler version 6.8.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QComboBox, QDateTimeEdit,
    QDialog, QDialogButtonBox, QHBoxLayout, QLabel,
    QLineEdit, QSizePolicy, QVBoxLayout, QWidget)

class Ui_DialogNewContract(object):
    def setupUi(self, DialogNewContract):
        if not DialogNewContract.objectName():
            DialogNewContract.setObjectName(u"DialogNewContract")
        DialogNewContract.resize(800, 300)
        self.verticalLayout = QVBoxLayout(DialogNewContract)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.labelDescr = QLabel(DialogNewContract)
        self.labelDescr.setObjectName(u"labelDescr")
        self.labelDescr.setWordWrap(True)

        self.verticalLayout.addWidget(self.labelDescr)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label = QLabel(DialogNewContract)
        self.label.setObjectName(u"label")

        self.horizontalLayout.addWidget(self.label)

        self.comboBoxProduct = QComboBox(DialogNewContract)
        self.comboBoxProduct.setObjectName(u"comboBoxProduct")

        self.horizontalLayout.addWidget(self.comboBoxProduct)


        self.verticalLayout.addLayout(self.horizontalLayout)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayoutFirstDeliveryTime = QHBoxLayout()
        self.horizontalLayoutFirstDeliveryTime.setObjectName(u"horizontalLayoutFirstDeliveryTime")
        self.label_3 = QLabel(DialogNewContract)
        self.label_3.setObjectName(u"label_3")

        self.horizontalLayoutFirstDeliveryTime.addWidget(self.label_3)

        self.lineFirstDeliveryNonFameTime = QDateTimeEdit(DialogNewContract)
        self.lineFirstDeliveryNonFameTime.setObjectName(u"lineFirstDeliveryNonFameTime")
        self.lineFirstDeliveryNonFameTime.setMinimumSize(QSize(300, 16777215))

        self.horizontalLayoutFirstDeliveryTime.addWidget(self.lineFirstDeliveryNonFameTime)

        self.lineFirstDeliveryTime = QLineEdit(DialogNewContract)
        self.lineFirstDeliveryTime.setObjectName(u"lineFirstDeliveryTime")

        self.horizontalLayoutFirstDeliveryTime.addWidget(self.lineFirstDeliveryTime)


        self.horizontalLayout_2.addLayout(self.horizontalLayoutFirstDeliveryTime)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.verticalLayout_label_4 = QVBoxLayout()
        self.verticalLayout_label_4.setObjectName(u"verticalLayout_label_4")
        self.verticalLayout_label_4.setAlignment(Qt.AlignTop)
        self.label_4 = QLabel(DialogNewContract)
        self.label_4.setObjectName(u"label_4")

        self.verticalLayout_label_4.addWidget(self.label_4)


        self.horizontalLayout_3.addLayout(self.verticalLayout_label_4)

        self.verticalLayout_delivery_interval_chooser = QVBoxLayout()
        self.verticalLayout_delivery_interval_chooser.setObjectName(u"verticalLayout_delivery_interval_chooser")
        self.verticalLayout_delivery_interval_chooser.setAlignment(Qt.AlignTop)
        self.delivery_interval_chooser = QWidget(DialogNewContract)
        self.delivery_interval_chooser.setObjectName(u"delivery_interval_chooser")

        self.verticalLayout_delivery_interval_chooser.addWidget(self.delivery_interval_chooser)


        self.horizontalLayout_3.addLayout(self.verticalLayout_delivery_interval_chooser)

        self.verticalLayout_lineDeliveryInterval = QVBoxLayout()
        self.verticalLayout_lineDeliveryInterval.setObjectName(u"verticalLayout_lineDeliveryInterval")
        self.verticalLayout_lineDeliveryInterval.setAlignment(Qt.AlignTop)
        self.lineDeliveryInterval = QLineEdit(DialogNewContract)
        self.lineDeliveryInterval.setObjectName(u"lineDeliveryInterval")
        self.lineDeliveryInterval.setMaximumSize(QSize(160, 16777215))

        self.verticalLayout_lineDeliveryInterval.addWidget(self.lineDeliveryInterval)

        self.labelErrorDeliveryInterval = QLabel(DialogNewContract)
        self.labelErrorDeliveryInterval.setObjectName(u"labelErrorDeliveryInterval")

        self.verticalLayout_lineDeliveryInterval.addWidget(self.labelErrorDeliveryInterval)


        self.horizontalLayout_3.addLayout(self.verticalLayout_lineDeliveryInterval)


        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.horizontalLayout_4 = QHBoxLayout()
        self.horizontalLayout_4.setObjectName(u"horizontalLayout_4")
        self.label_5 = QLabel(DialogNewContract)
        self.label_5.setObjectName(u"label_5")

        self.horizontalLayout_4.addWidget(self.label_5)

        self.lineExpirationTimeNonFameTime = QDateTimeEdit(DialogNewContract)
        self.lineExpirationTimeNonFameTime.setObjectName(u"lineExpirationTimeNonFameTime")
        self.lineExpirationTimeNonFameTime.setMinimumSize(QSize(300, 16777215))

        self.horizontalLayout_4.addWidget(self.lineExpirationTimeNonFameTime)

        self.lineExpirationTime = QLineEdit(DialogNewContract)
        self.lineExpirationTime.setObjectName(u"lineExpirationTime")

        self.horizontalLayout_4.addWidget(self.lineExpirationTime)


        self.verticalLayout.addLayout(self.horizontalLayout_4)

        self.buttonBox = QDialogButtonBox(DialogNewContract)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)


        self.retranslateUi(DialogNewContract)
        self.buttonBox.accepted.connect(DialogNewContract.accept)
        self.buttonBox.rejected.connect(DialogNewContract.reject)

        QMetaObject.connectSlotsByName(DialogNewContract)
    # setupUi

    def retranslateUi(self, DialogNewContract):
        DialogNewContract.setWindowTitle(QCoreApplication.translate("DialogNewContract", u"Dialog", None))
        self.labelDescr.setText(QCoreApplication.translate("DialogNewContract", u"<html><head/><body><p>Details of the <span style=\"\n"
"                            font-weight:600;\">new contract</span> between agent #XXX and agent #YYY:</p></body></html>\n"
"                        ", None))
        self.label.setText(QCoreApplication.translate("DialogNewContract", u"Product:", None))
        self.label_3.setText(QCoreApplication.translate("DialogNewContract", u"First delivery time:", None))
        self.label_4.setText(QCoreApplication.translate("DialogNewContract", u"Delivery interval in steps:", None))
        self.labelErrorDeliveryInterval.setText("")
        self.labelErrorDeliveryInterval.setStyleSheet(QCoreApplication.translate("DialogNewContract", u"color: red;", None))
        self.label_5.setText(QCoreApplication.translate("DialogNewContract", u"Expiration time:", None))
    # retranslateUi

