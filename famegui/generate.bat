@echo off

setlocal
set PATH=%~dp0\..\..\..\Python391\Scripts;%PATH%

set OUTDIR=%~dp0generated

if not exist %OUTDIR% mkdir %OUTDIR%

pyside2-rcc %~dp0qt_resources.qrc -o %OUTDIR%/qt_resources_rc.py || GOTO:FAILURE
pyside2-uic --from-imports %~dp0mainwindow.ui -o %OUTDIR%/ui_mainwindow.py || GOTO:FAILURE
pyside2-uic --from-imports %~dp0dialog_scenario_properties.ui -o %OUTDIR%/ui_dialog_scenario_properties.py || GOTO:FAILURE
pyside2-uic --from-imports %~dp0dialog_newagent.ui -o %OUTDIR%/ui_dialog_newagent.py || GOTO:FAILURE
pyside2-uic --from-imports %~dp0dialog_newcontract.ui -o %OUTDIR%/ui_dialog_newcontract.py || GOTO:FAILURE

GOTO:EOF
:FAILURE
echo Generation failed! Make sure your PySide2 installation is in your PATH.
