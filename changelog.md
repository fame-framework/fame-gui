# Changelog
## [0.9.0] - 2025-01-20
### Added
- Option to multi-edit Agents, #77
- Option to multi-edit Contracts, #54
- Option to Undo/Redo changes when editing a scenario, #80
- Custom presets for attribute `DeliveryInterval`, #53
- Central file for configuration items, such as keymaps and color schemes, #72
- Option to change agent type colors from within the program, #13
- Timeseries plot for agents with a timeseries, #30
- Curved contract lines for better identification of individual contracts, #89
- Autosaving when compiling a protobuf file, #81
- Possibility to copy logs from console, #86
- Support to allow zooming via trackpads, #83

### Changed
- Update to FAME-Io v3.0, #73
- Update to PySide6, #74
- Improved spacing in EditAgent window, #84
- Improved spacing of canvas and console panels, #82
- Improved spacing for list elements in New Agent window, #76
- Improved icon when dragging canvas, #78
- Close program without any warning when no changes are found in scenario, #75

### Fixed
- Fix to save agent position in scenario.yaml, #90

# Changelog
## [0.8.0] - 2023-11-15
### Added
- Option to edit/delete Agents and Contracts
- Option to edit nested Block type attributes in Agents

## [0.7.0] - 2022-11-28
### Added
- Show HelpText on Attributes if available
- Create `n-to-1`, `1-to-n`, and `n-to-n` contracts by selecting agents holding `shift` key
- Add a validation function to display all the validation errors
- List of recently opened projects
- Highlighting of contracts of selected agent on canvas

### Changed
- Distribute agents on canvas avoiding overlaps by allowing auto-arrange function
- When creating a new scenario, the schema is pre-selected automatically if it is the only one available
- Move graph view pane with mouse
- Disabled the `close project` action when no project is open

## [0.6.0] - 2022-04-07
### Changed
- FameGUI is now using FameIO to manage the scenario data (loading, validation, saving) rather than doing it itself
- The agent creation dialog has been updated to (partially) support the new types introduced in FameIO
- The agent creation dialog now also accepts a single float value for timeseries fields

## [0.5.1] - 2021-05-11
### Changed
- Allow contract field  `FirstDeliveryTime` to be negative

## [0.5] - 2021-04-23
### Added
- Allow zooming using the mouse wheel (while holding the CTRL key)
### Changed
- Allow the user to enter numbers of 20 digits for contract fields `DeliveryIntervalInSteps` and `FirstDeliveryTime`
- Fix invalid saving in YAML files of the contract fields `DeliveryIntervalInSteps` and `FirstDeliveryTime`
- Ensure minimal Python version (3.7) is checked by `pip` before installing `famegui` package
- Replaced embedded schema with a new demo one
- Properly update the modified X,Y positions of the agents in the output file
- Automatically adjust the scene size to contain all agents (when creating a new agent or when saving the project)

## [0.4] - 2021-03-21
### Added
- Timeseries CSV files should be located in the `timeseries` directory in the working dir
- The agent dialog box allows to fill all the attributes for the new agent
- Scenario field `GeneralProperties` is supported in yaml files and can be edited via a new dialog box
- Protobuf output files are located by default in the `protobuf` directory in the working dir
- Allow to generate protobuf output from the current scenario (with proper support of the `!include` yaml extension)
### Changed
- Accept to open a scenario file that does not fulfill the attached schema (display a warning message in such case)
- Format of schema file has been updated based on fameio v1.2.3 (use the term `Attribute` rather than `Field`)

## [0.3] - 2021-02-12
### Added
- Request the user to configure a working directory where to put / search all the files needed by the application
- Install a default schema file in the FAME working dir if none available
- Request the user to select a valid schema file if the one specified in the schenario file is missing
- Better log output: more information displayed in a colored way
- Improved error handling by displaying an error dialog box for uncaught exceptions
- Save agent attributes and display coords (X, Y) to yaml output
### Changed
- Allow scenario YAML files to be empty
- Improved the way unsaved scenario changes are handled
- Configure the open and save dialog boxes to select the `scenarios` directory in the working dir
- The add new agent dialog is now using the schema attached to the scenario for the attributes to display

## [0.2] - 2021-01-27
### Added
- New python package `famegui.models` to manipulate the data from a scenario.yaml file
- New python package `famegui` that contains the GUI application
