from PySide6.QtWidgets import QWidget, QVBoxLayout, QApplication, QSizePolicy
from PySide6.QtCore import Qt, QSize


class SquareWidget(QWidget):
    def __init__(self):
        super().__init__()
        # Fixed height of 200px
        self.setFixedHeight(200)
        # Make it match width to height
        self.setFixedWidth(200)
        # This ensures the widget stays square in layouts
        self.setSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)

    def sizeHint(self):
        # Provide a square size hint
        return QSize(200, 200)


# Example usage
if __name__ == '__main__':
    app = QApplication([])
    window = QWidget()
    layout = QVBoxLayout(window)

    square = SquareWidget()
    layout.addWidget(square, alignment=Qt.AlignLeft)  # Align left in the layout

    window.resize(400, 300)
    window.show()
    app.exec()